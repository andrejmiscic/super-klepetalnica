/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

//boolean, ki je true, ce se posilja slika
var jeSlika = false;

//ce besedilo vsebuje slilke, jih obda z <img>
function dodajSlike(besedilo){
  var besede = besedilo.split(/[ ]+/);
  var limit = besede.length;
  
  for(var i = 0; i < limit; i++){
    var zacetek = besede[i].indexOf("http");
    if(zacetek > -1){
      if(besede[i].indexOf(".png") > -1){
        jeSlika = true;
        var konec = besede[i].indexOf(".png");
        var link = besede[i].substring(zacetek, konec + 4);
        besede.push("<br /><img src='" + link + "' style='width: 200px; margin-left: 20px' />");
      } else if(besede[i].indexOf(".jpg") > -1){
        jeSlika = true;
        var konec = besede[i].indexOf(".jpg");
        var link = besede[i].substring(zacetek, konec + 4);
        besede.push("<br /><img src='" + link + "' style='width: 200px; margin-left: 20px' />");
      } else if(besede[i].indexOf(".gif") > -1){
        jeSlika = true;
        var konec = besede[i].indexOf(".gif");
        var link = besede[i].substring(zacetek, konec + 4);
        besede.push("<br /><img src='" + link + "' style='width: 200px; margin-left: 20px' />");
      }
    }
  }
  besedilo = besede.join(' ');
  return besedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko || jeSlika) {
    jeSlika = false;
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />")
               .split("&lt;br /&gt;").join("<br />")
               .split("20px' /&gt;").join("20px' />");
    return divElementHtmlTekst(sporocilo);
  }  else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * else if(jeSlika){
    return divElementHtmlTekst(sporocilo);
  }
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

//boolean, ki je true, ce so poslali krc
var krc = false;
//array, ki hrani vse vzdevke in nadimke
var vzdevki = [];

/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSlike(sporocilo);
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      var besede = sistemskoSporocilo.split('\"');
      if(besede[0] == "preimenovanje"){
        var zeObstaja = false;
        if(besede[1] == trenutniVzdevek){
          sistemskoSporocilo = "Sebe ne moreš preimenovati, lahko pa si spremeniš vzdevek.";
        } else {
        mainloop:
        for(var i = 0; i < vzdevki.length; i++){
          if(vzdevki[i][0] == besede[1]){
            vzdevki[i][1] = besede[3];
            zeObstaja = true;
            break mainloop;
          }
        }
        if(!zeObstaja){
          vzdevki.push([besede[1], besede[3]]);
        }
        klepetApp.posodobiVzdevke(vzdevki);
        sistemskoSporocilo = "Uporabnika " + besede[1] + " si preimenoval v " + besede[3] + ".";
        }
        
      }
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Počakaj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    if(sporocilo.besedilo.indexOf("style='width: 200px; margin-left: 20px' />") > -1){
      jeSlika = true;
    }
    
    mainloop:
    for(var j = 0; j < vzdevki.length; j++){
      if(vzdevki[j][0] == sporocilo.besedilo.substring(0, vzdevki[j][0].length)){
        sporocilo.besedilo = sporocilo.besedilo.replace(vzdevki[j][0], vzdevki[j][1] + " (" + vzdevki[j][0] + ")");
        break mainloop;
      }
    }
    
    var novElement;
    if(krc || sporocilo.besedilo.indexOf("%krc%") > -1){
      sporocilo.besedilo = sporocilo.besedilo.replace("%krc%", "&#9756;");
      novElement = divElementHtmlTekst(sporocilo.besedilo);
      krc = false;
    } else {
      novElement = divElementEnostavniTekst(sporocilo.besedilo);
    }
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      var ime = uporabniki[i];
      mainloop:
      for(var j = 0; j < vzdevki.length; j++){
        if(vzdevki[j][0] == ime){
           ime = vzdevki[j][1] + " (" + vzdevki[j][0] + ")";
           break mainloop;
        }
      }
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(ime));
    }
    
    //klik na ime uporabnika le temu poslje krc
    $('#seznam-uporabnikov div').click(function(){
      var name;
      if($(this).text().indexOf("(") > -1){
        name = $(this).text().substring($(this).text().indexOf("(") + 1, $(this).text().indexOf(")"));
      } else{
        name = $(this).text();
      }
      var sporocilo = klepetApp.procesirajUkaz('/zasebno "' + name + '" "&#9756;"');
      $('#sporocila').append(divElementHtmlTekst(sporocilo));
      krc = true;
   });
  });
  
  //sistemski socket, ki obvesca, ce se kdo preimenuje
  socket.on('sistemski', function(sporocilo) {
     var besede = sporocilo.split('\"');
     if(besede[0] == "preimenovanje"){
       for(var i = 0; i < vzdevki.length; i++){
         if(vzdevki[i][0] == besede[1]){
           vzdevki[i][0] = besede[3];
         }
       }
       klepetApp.posodobiVzdevke(vzdevki);
     }
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
